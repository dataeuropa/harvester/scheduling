# ChangeLog

## Unreleased

## 2.1.2 (2023-11-11)

**Changed:**
* Dependencies update

## 2.1.1 (2023-11-08)

**Added:**
* `podSecurityContext` & `containerSecurityContext` to helm chart to be able to automatically create the database file

**Changed:**
* `README.md` update

## 2.1.0 (2023-05-14)

**Added:**
* `pipes` definitions to helm chart
* Upload pipe API endpoint
* Logstash log output for major events 

**Changed:**
* Use new launcher lib

## 2.0.2 (2022-12-16)

**Changed:**
* Bulk trigger update error response
* Default log level to info

**Fixed:**
* End CLI command `show` when pipe not found
* Setting a second and more trigger per pipe
* Set correct next fire time in returned trigger objects

## 2.0.1 (2022-10-11)

**Fixed:**
* Immediate triggers do not require explicit trigger type `immediate`

## 2.0.0 (2022-10-11)

**Changed:**
* Honor the launch return value as JsonObject
* API refactoring

## 1.2.6 (2022-02-11)

**Added:**
* Helm chart update pipeline trigger

**Changed:**
* Sort pipes in shell alphabetically

## 1.2.5 (2021-06-06)

**Fixed:**
* Build process

## 1.2.4 (2021-06-05)

**Changed:**
* Lib dependencies

## 1.2.3 (2021-05-06)

**Added:**
* Configurable application port

## 1.2.2 (2021-03-04)

**Fixed:**
* Configurable logo and favicon

## 1.2.1 (2021-03-02)

**Added:**
* Configurable logo and favicon

## 1.2.0 (2021-02-06)

**Changed:**
* Response handling of API

**Added:**
* Completion feature for shell commands
* Listing of all triggers in the shell

## 1.1.0 (2020-07-30)

**Added:**
* Reading json, yaml and properties configurations from config directory
* Reading yaml pipes
* Git configuration for a path in repository 
* Support for yaml pipe definitions

## 1.0.4 (2020-07-28)

**Changed:**
* Lib dependencies

## 1.0.3 (2020-02-28)

**Fixed:**
* Downgrade H2 due to incompatibility problems

## 1.0.2 (2020-02-28)

**Changed:**
* Lib updates

## 1.0.1 (2019-12-10)

**Added:**
* Shell command `pipe` for list of pipes, launch a pipe and show a pipe
* Expose port 5000 in Dockerfile

**Changed:**
* Shell command `trigger` refactored

## 1.0.0 (2019-11-08)

**Added:**
* `PIVEAU_LOG_LEVEL` for configuring the global log level of the `io.piveau` package
* `PIVEAU_` prefix to logstash configuration environment variables
* `PIVEAU_CLUSTER_CONFIG` for configuring piveau cluster. See pipe launcher for config schema

**Changed:**
* Use `PipeLauncher` for running pipes
* Requires now latest LTS Java 11
* Docker base image to openjdk:11-jre

**Removed:**
* `config` element of trigger. Utilize `configs` to apply a config to all segments.
* Obsolete classes `ServiceDiscovery` and `GitRepository` 
* `piveau-exporting-hub` special handling

**Fixed:**
* Update all dependencies

## 0.1.1 (2019-07-11)

**Fixed:**
* Pipe configs handling

## 0.1.0 (2019-05-17)

**Added:**
* A service discovery mechanism
* Log config at startup (debug)

**Removed:**
* Environment configuration `PIVEAU_HUB_ADDRESS`. Deprecated due to new service discovery feature

## 0.0.1 (2019-05-03)

Initial release
