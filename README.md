# piveau scheduling
Microservice for scheduling pipes.

## Table of Contents
1. [Build](#build)
1. [Run](#run)
1. [Interface](#interface)
1. [Docker](#docker)
1. [Configuration](#configuration)
    1. [Environment](#environment)
    1. [Logging](#logging)
1. [License](#license)

## Build
Requirements:
 * Git
 * Maven 3
 * Java 17

```bash
$ git clone <gitrepouri>
$ cd piveau-consus-scheduling
$ mvn package
```

## Run

```bash
$ java -jar target/scheduling.jar
```

## Interface

The documentation of the REST service can be found when the root context is opened in a browser. It is based on the OpenAPI specification stored at `src/main/resources/webroot/openapi.yaml`.

```
http://localhost:8080/
```

## Shell

The Shell can be found under the configured port, for example:

```
http://localhost:8085/shell.html
```

## Docker

Build docker image:
```bash
$ docker build -t piveau/piveau-consus-scheduling .
```

Run docker image:
```bash
$ docker run -it -p 8080:8080 piveau/piveau-consus-scheduling
```

## Configuration

### Environment
See also [piveau launcher]() for more.

| Variable                | Description                                                           | Default Value                                                         |
|:------------------------|:----------------------------------------------------------------------|:----------------------------------------------------------------------|
| `PORT`                  | Port this service will run on.                                        | `8080`                                                                |
| `PIVEAU_CLUSTER_CONFIG` | Json object describing the mapping between service name and endpoint. | `{}` For details, see [PIVEAU_CLUSTER_CONFIG](#piveau_cluster_config) |
| `PIVEAU_SHELL_CONFIG`   | Json object describing shell endpoints.                               | `{}` For details, see [PIVEAU_SHELL_CONFIG](#piveau_shell_config)     |
| `PIVEAU_FAVICON_PATH`   | Path to favicon, used in OpenAPI rendering.                           | `"webroot/images/favicon.png"`                                        |
| `PIVEAU_LOGO_PATH`      | Path to logo, used in OpenAPI rendering.                              | `"webroot/images/logo.png"`                                           |

#### PIVEAU_CLUSTER_CONFIG

Configuring pipe repositories (`pipeRepositories`) and pipe service endpoints (`serviceDiscovery`).

```json
{
   "pipeRepositories": {

   },
   "serviceDiscovery": {

   }
}
```

Pipe repositories are a map of named locations for json files. The repo named `resources` should contain paths loaded from the classpath, all others are treated as git repositories.
```json
{
  "pipeRepositories": {
    "resources": {
      "paths": [
        "pipes"
      ]
    },
    "system": {
      "uri": "",
      "branch": "master",
      "username": "",
      "token": ""
    }
  }
}
``` 

The service discovery is a JSON map of segment names to endpoints. Currently `http` and `eventbus` endpoints are supported.
```json
{
  "serviceDiscovery": {
    "example1-segment": {
      "http": {
        "method": "POST",
        "address": "http://example.com:8080/pipe"
      },
      "eventbus": {
        "address": "piveau.pipe.test.queue"
      }
    },
    "example2-segment": {
      
    }
  }
}
```

#### PIVEAU_SHELL_CONFIG

Enables browser (`http`) or shell (`telnet`) based command line interface access. Consider to set host to `"localhost"` for security reasons.

Example:

```json
{
   "http": {
      "host": "0.0.0.0",
      "port": 8085
   },
   "telnet": {
      "host": "0.0.0.0",
      "port": 5000
   }
}
```

### Logging
See [logback](https://logback.qos.ch/documentation.html) documentation for more details

| Variable                     | Description                                                                                             | Default Value                         |
|:-----------------------------|:--------------------------------------------------------------------------------------------------------|:--------------------------------------|
| `PIVEAU_CONSUS_LOG_APPENDER` | Configures the log appender for the consus context. possible values are `STDOUT`, `LOGSTASH`, or `FILE` | `STDOUT`                              |
| `PIVEAU_LOGSTASH_HOST`       | The host of the logstash service                                                                        | `logstash`                            |
| `PIVEAU_LOGSTASH_PORT`       | The port the logstash service is running                                                                | `5044`                                |
| `PIVEAU_CONSUS_LOG_PATH`     | Path to the file for the file appender                                                                  | `logs/piveau-pipe.%d{yyyy-MM-dd}.log` |
| `PIVEAU_CONSUS_LOG_LEVEL`    | The log level for the consus context                                                                    | `INFO`                                |
| `PIVEAU_LOG_LEVEL`           | The log level for class contexts                                                                        | `INFO`                                |

## License

[Apache License, Version 2.0](LICENSE.md)
