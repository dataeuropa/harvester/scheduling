package io.piveau.scheduling.quartz;

import io.piveau.scheduling.launcher.LauncherService;
import io.piveau.utils.PiveauContext;
import io.vertx.core.json.JsonObject;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@DisallowConcurrentExecution
public class PipeJob implements Job {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final LauncherService launcherService;
    private final String pipeName;

    private final PiveauContext piveauContext = new PiveauContext("consus", "scheduling");

    public PipeJob(LauncherService launcherService, String pipeName) {
        this.launcherService = launcherService;
        this.pipeName = pipeName;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {

        String triggerObject = jobExecutionContext.getMergedJobDataMap().getString("triggerObject");
        log.debug("Job triggered: {}", triggerObject);

        JsonObject trigger = new JsonObject(triggerObject);

        JsonObject configs = trigger.getJsonObject("configs", new JsonObject());
        String triggerId = jobExecutionContext.getTrigger().getKey().getName();

        PiveauContext triggerContext = piveauContext.extend(pipeName + "/" + triggerId);
        launcherService.launch(pipeName, configs)
                .onSuccess(runId -> {
                    triggerContext.log().info("Start pipe: successful");
                    log.debug("Pipe {} started successfully ({})!", pipeName, runId.getString("runId"));
                })
                .onFailure(cause -> {
                    triggerContext.log().error("Start pipe", cause);
                    log.error("Starting pipe " + pipeName + " failed!", cause);
                });
    }

}
