package io.piveau.scheduling.launcher;

import io.piveau.pipe.PipeLauncher;
import io.piveau.pipe.PiveauCluster;
import io.piveau.pipe.model.ModelKt;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceException;

import java.util.List;

public class LauncherServiceImpl implements LauncherService {

    private PipeLauncher launcher;

    private final Vertx vertx;

    LauncherServiceImpl(Vertx vertx, PiveauCluster cluster, Handler<AsyncResult<LauncherService>> readyHandler) {
        this.vertx = vertx;

        cluster.pipeLauncher(vertx)
                .onSuccess(launcher -> {
                    this.launcher = launcher;
                    readyHandler.handle(Future.succeededFuture(this));
                })
                .onFailure(t -> readyHandler.handle(Future.failedFuture(t)));
    }

    @Override
    public Future<JsonObject> launch(String pipeName, JsonObject configs) {
        if (launcher.isPipeAvailable(pipeName)) {
            return launcher.runPipe(pipeName, configs, null);
        } else {
            return Future.failedFuture(new ServiceException(404, "Pipe not found"));
        }
    }

    @Override
    public Future<JsonObject> status(String pipeName, String runId) {
        return Future.future(promise -> {
            if (launcher.isPipeAvailable(pipeName)) {
                launcher.getRunStatus(pipeName, runId).onComplete(promise);
            } else {
                promise.fail(new ServiceException(404, "Pipe not found"));
            }
        });
    }

    @Override
    public Future<Void> cancel(String pipeName, String runId) {
        return Future.future(promise -> {
            if (launcher.isPipeAvailable(pipeName)) {
                launcher.cancelRun(pipeName, runId).onComplete(promise);
            } else {
                promise.fail(new ServiceException(404, "Pipe not found"));
            }
        });
    }

    @Override
    public Future<Boolean> isPipeAvailable(String pipeName) {
        return Future.succeededFuture(launcher.isPipeAvailable(pipeName));
    }

    @Override
    public Future<JsonObject> getPipe(String pipeName) {
        if (launcher.isPipeAvailable(pipeName)) {
            return Future.succeededFuture(new JsonObject(ModelKt.prettyPrint(launcher.getPipe(pipeName))));
        } else {
            return Future.failedFuture(new ServiceException(404, "Pipe not found"));
        }
    }

    @Override
    public Future<JsonObject> putPipe(String uploadedFilename, String filename) {
        return launcher.addPipe(uploadedFilename, filename);
    }

    @Override
    public Future<List<JsonObject>> availablePipes() {
        try {
            List<JsonObject> list = launcher.availablePipes().stream().map(p -> new JsonObject(ModelKt.prettyPrint(p))).toList();
            return Future.succeededFuture(list);
        } catch (Throwable t) {
            return Future.failedFuture(t);
        }
    }

    @Override
    public Future<List<String>> availablePipeNames() {
        try {
            return Future.succeededFuture(launcher.availablePipes().stream().map(pipe -> pipe.getHeader().getName()).toList());
        } catch (Throwable t) {
            return Future.failedFuture(t);
        }
    }

}
