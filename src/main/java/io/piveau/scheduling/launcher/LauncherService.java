package io.piveau.scheduling.launcher;

import io.piveau.pipe.PiveauCluster;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;

import java.util.List;

@ProxyGen
public interface LauncherService {
    String SERVICE_ADDRESS = "io.piveau.scheduling.launcher.service";

    static LauncherService create(Vertx vertx, PiveauCluster cluster, Handler<AsyncResult<LauncherService>> readyHandler) {
        return new LauncherServiceImpl(vertx, cluster, readyHandler);
    }

    static LauncherService createProxy(Vertx vertx, String address) {
        return new LauncherServiceVertxEBProxy(vertx, address);
    }

    Future<JsonObject> launch(String pipeName, JsonObject configs);

    Future<JsonObject> status(String pipeName, String runId);

    Future<Void> cancel(String pipeName, String runId);

    Future<Boolean> isPipeAvailable(String pipeName);

    Future<JsonObject> getPipe(String pipeName);

    Future<JsonObject> putPipe(String uploadedFilename, String filename);

    Future<List<JsonObject>> availablePipes();

    Future<List<String>> availablePipeNames();

}
