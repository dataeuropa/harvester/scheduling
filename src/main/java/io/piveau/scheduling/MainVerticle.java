package io.piveau.scheduling;

import io.piveau.scheduling.launcher.LauncherService;
import io.piveau.scheduling.launcher.LauncherServiceVerticle;
import io.piveau.scheduling.quartz.QuartzService;
import io.piveau.scheduling.quartz.QuartzServiceVerticle;
import io.piveau.scheduling.shell.ShellVerticle;
import io.piveau.utils.ConfigurableAssetHandler;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.*;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.healthchecks.HealthCheckHandler;
import io.vertx.ext.healthchecks.Status;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.openapi.RouterBuilder;
import io.vertx.ext.web.validation.RequestParameters;
import io.vertx.ext.web.validation.ValidationHandler;
import io.vertx.serviceproxy.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReference;

import static io.piveau.scheduling.ApplicationConfig.*;

public class MainVerticle extends AbstractVerticle {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private QuartzService quartzService;

    private LauncherService launcherService;

    private JsonObject config;

    @Override
    public void start(Promise<Void> startPromise) {
        ConfigStoreOptions envStoreOptions = new ConfigStoreOptions()
                .setType("env")
                .setConfig(new JsonObject().put("keys", new JsonArray()
                        .add(ENV_APPLICATION_PORT)
                        .add(ENV_PIVEAU_CLUSTER_CONFIG)
                        .add(ENV_PIVEAU_SHELL_CONFIG)
                        .add(ENV_PIVEAU_FAVICON_PATH)
                        .add(ENV_PIVEAU_LOGO_PATH)));

        ConfigStoreOptions fileStoreOptions = new ConfigStoreOptions()
                .setType("directory")
                .setConfig(new JsonObject()
                        .put("path", "config")
                        .put("filesets", new JsonArray()
                                .add(new JsonObject()
                                        .put("format", "json")
                                        .put("pattern", "*.json"))
                                .add(new JsonObject()
                                        .put("format", "yaml")
                                        .put("pattern", "*.yaml"))
                                .add(new JsonObject()
                                        .put("format", "yaml")
                                        .put("pattern", "*.yml"))
                                .add(new JsonObject()
                                        .put("format", "properties")
                                        .put("pattern", "*.properties"))));

        ConfigRetriever retriever = ConfigRetriever.create(vertx, new ConfigRetrieverOptions()
                .addStore(envStoreOptions)
                .addStore(fileStoreOptions));

        AtomicReference<String> logoPath = new AtomicReference<>();
        AtomicReference<String> faviconPath = new AtomicReference<>();

        retriever.getConfig()
                .compose(configuration -> {
                    config = configuration;
                    log.info(config.encodePrettily());

                    faviconPath.set(config.getString(ENV_PIVEAU_FAVICON_PATH, DEFAULT_PIVEAU_FAVICON_PATH));
                    logoPath.set(config.getString(ENV_PIVEAU_LOGO_PATH, DEFAULT_PIVEAU_LOGO_PATH));

                    Promise<JsonObject> launcherPromise = Promise.promise();
                    vertx.deployVerticle(LauncherServiceVerticle.class, new DeploymentOptions().setWorker(true).setConfig(config))
                            .onSuccess(id -> launcherPromise.complete(config))
                            .onFailure(launcherPromise::fail);

                    return launcherPromise.future();
                })
                .compose(config -> {
                    Future<String> quartzFuture = vertx.deployVerticle(QuartzServiceVerticle.class, new DeploymentOptions().setWorker(true).setConfig(config));
                    Future<String> shellFuture = vertx.deployVerticle(ShellVerticle.class, new DeploymentOptions().setWorker(true).setConfig(config));

                    return CompositeFuture.all(quartzFuture, shellFuture);
                })
                .compose(v -> {
                    quartzService = QuartzService.createProxy(vertx, QuartzService.SERVICE_ADDRESS);
                    launcherService = LauncherService.createProxy(vertx, LauncherService.SERVICE_ADDRESS);

                    return RouterBuilder.create(vertx, "webroot/openapi.yaml");
                })
                .onSuccess(builder -> {
//                    builder.rootHandler(CorsHandler.create().addRelativeOrigin("*").allowedHeader("Content-Type").allowedMethods(Stream.of(HttpMethod.PUT, HttpMethod.GET).collect(Collectors.toSet())));
                    builder.rootHandler(BodyHandler.create());
                    builder.rootHandler(StaticHandler.create());

                    builder.operation("listPipes").handler(this::handleListPipes);
                    builder.operation("getPipe").handler(this::handleGetPipe);
                    builder.operation("putPipe")
//                            .handler(new PermissionHandler(List.of(
//                                    Constants.AUTH_SCOPE_PIPE_CREATE,
//                                    Constants.AUTH_SCOPE_PIPE_UPDATE)))
                            .handler(this::handlePutPipe);
                    builder.operation("listTriggers").handler(this::handleListTriggers);
                    builder.operation("deleteTriggers").handler(this::handleDeleteTriggers);
                    builder.operation("getTrigger").handler(this::handleGetTrigger);
                    builder.operation("putTrigger").handler(this::handlePutTrigger);
                    builder.operation("patchTrigger").handler(this::handlePatchTrigger);
                    builder.operation("deleteTrigger").handler(this::handleDeleteTrigger);

                    builder.operation("mapTriggers").handler(this::handleMapTriggers);
                    builder.operation("putTriggers").handler(this::handlePutTriggers);

                    Router router = builder.createRouter();

                    WebClient client = WebClient.create(vertx);
                    router.route("/images/logo").handler(new ConfigurableAssetHandler(logoPath.get(), client));
                    router.route("/images/favicon").handler(new ConfigurableAssetHandler(faviconPath.get(), client));

                    HealthCheckHandler hch = HealthCheckHandler.create(vertx);
                    hch.register("buildInfo", future -> vertx.fileSystem().readFile("buildInfo.json", bi -> {
                        if (bi.succeeded()) {
                            future.complete(Status.OK(bi.result().toJsonObject()));
                        } else {
                            future.fail(bi.cause());
                        }
                    }));
                    router.get("/health").handler(hch);

                    vertx.createHttpServer()
                            .requestHandler(router)
                            .listen(config.getInteger(ENV_APPLICATION_PORT, DEFAULT_APPLICATION_PORT))
                            .onSuccess(success -> startPromise.complete())
                            .onFailure(startPromise::fail);
                })
                .onFailure(startPromise::fail);
    }

    private void handleListPipes(RoutingContext routingContext) {
        launcherService.availablePipeNames()
                .onSuccess(list -> routingContext.response()
                        .setStatusCode(200)
                        .end(new JsonArray(list).encodePrettily())
                )
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handleGetPipe(RoutingContext routingContext) {
        RequestParameters params = routingContext.get(ValidationHandler.REQUEST_CONTEXT_KEY);
        String pipeName = params.pathParameter("pipeName").getString();
        launcherService.getPipe(pipeName)
                .onSuccess(pipe -> routingContext.response()
                        .setStatusCode(200)
                        .end(pipe.encodePrettily()))
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handlePutPipe(RoutingContext routingContext) {
        if (routingContext.fileUploads().size() != 1) {
            routingContext.response().setStatusCode(400).end("Only one file allowed in upload");
            return;
        }

        FileUpload fileUpload = routingContext.fileUploads().get(0);

        launcherService.putPipe(fileUpload.uploadedFileName(), fileUpload.fileName())
                .onSuccess(result -> {
                    switch (result.getString("status", "")) {
                        case "added" -> routingContext.response().setStatusCode(200)
//                                .putHeader(HttpHeaders.LOCATION, result.getString("location"))
                                .end();
                        case "created" -> routingContext.response().setStatusCode(201)
//                                .putHeader(HttpHeaders.LOCATION, result.getString("location"))
                                .end();
                        case "updated" -> routingContext.response().setStatusCode(204).end();
                        default -> routingContext.response().setStatusCode(500).end();
                    }
                })
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handleListTriggers(RoutingContext routingContext) {
        RequestParameters params = routingContext.get(ValidationHandler.REQUEST_CONTEXT_KEY);
        String pipeName = params.pathParameter("pipeName").getString();
        quartzService.getTriggers(pipeName)
                .onSuccess(result -> routingContext.response()
                        .setStatusCode(200)
                        .end(result.encodePrettily()))
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handleDeleteTriggers(RoutingContext routingContext) {
        RequestParameters params = routingContext.get(ValidationHandler.REQUEST_CONTEXT_KEY);
        String pipeName = params.pathParameter("pipeName").getString();
        quartzService.deleteTriggers(pipeName)
                .onSuccess(v -> routingContext.response().setStatusCode(204).end())
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handleGetTrigger(RoutingContext routingContext) {
        RequestParameters params = routingContext.get(ValidationHandler.REQUEST_CONTEXT_KEY);
        String pipeName = params.pathParameter("pipeName").getString();
        String triggerId = params.pathParameter("triggerId").getString();
        quartzService.getTrigger(pipeName, triggerId)
                .onSuccess(trigger -> routingContext.response()
                        .setStatusCode(200)
                        .end(trigger.encodePrettily())
                )
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handlePutTrigger(RoutingContext routingContext) {
        RequestParameters params = routingContext.get(ValidationHandler.REQUEST_CONTEXT_KEY);
        String pipeName = params.pathParameter("pipeName").getString();
        String triggerId = params.pathParameter("triggerId").getString();
        JsonObject trigger = params.body().getJsonObject();
        quartzService.putTrigger(pipeName, triggerId, trigger)
                .onSuccess(statusCode -> {
                    int code = "created".equalsIgnoreCase(statusCode) ? 201 : 200;
                    routingContext.response().setStatusCode(code).end();
                })
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handleDeleteTrigger(RoutingContext routingContext) {
        RequestParameters params = routingContext.get(ValidationHandler.REQUEST_CONTEXT_KEY);
        String pipeName = params.pathParameter("pipeName").getString();
        String triggerId = params.pathParameter("triggerId").getString();
        quartzService.deleteTrigger(pipeName, triggerId)
                .onSuccess(v -> routingContext.response().setStatusCode(204).end())
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handlePatchTrigger(RoutingContext routingContext) {
        RequestParameters params = routingContext.get(ValidationHandler.REQUEST_CONTEXT_KEY);
        String pipeName = params.pathParameter("pipeName").getString();
        String triggerId = params.pathParameter("triggerId").getString();

        JsonObject patch = params.body().getJsonObject();

        quartzService.patchTrigger(pipeName, triggerId, patch)
                .onSuccess(result -> routingContext.response()
                        .setStatusCode(200)
                        .end(result.encodePrettily()))
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handleMapTriggers(RoutingContext routingContext) {
        quartzService.listTriggers()
                .onSuccess(result -> routingContext.response()
                        .setStatusCode(200)
                        .end(result.encodePrettily()))
                .onFailure(cause -> serviceException(cause, routingContext));
    }

    private void handlePutTriggers(RoutingContext routingContext) {
        RequestParameters params = routingContext.get(ValidationHandler.REQUEST_CONTEXT_KEY);
        boolean resetPipe = params.queryParameter("resetPipes").getBoolean();
//        boolean resetPipe = !routingContext.queryParams().contains("resetPipes") && Boolean.parseBoolean(routingContext.queryParams().get("resetPipes"));
        JsonObject bulk = params.body().getJsonObject();

        ArrayList<Future<String>> futureList = new ArrayList<>();

        bulk.fieldNames().forEach(pipeName -> {
            Future.<String>future(promise -> {
                if (resetPipe) {
                    quartzService.deleteTriggers(pipeName).map(v -> pipeName).onComplete(promise);
                } else {
                    promise.complete(pipeName);
                }
            })
            .onSuccess(name -> {
                JsonArray triggers = bulk.getJsonArray(name);
                triggers.stream()
                        .map(JsonObject.class::cast)
                        .forEach(trigger -> {
                            String triggerId = trigger.getString("id");
                            futureList.add(quartzService.putTrigger(name, triggerId, trigger));
                        });
            })
            .onFailure(cause -> {});
        });
        CompositeFuture.join(new ArrayList<>(futureList))
                .onSuccess(v -> routingContext.response().setStatusCode(204).end())
                .onFailure(cause -> {
                    JsonArray errors = new JsonArray();
                    futureList.stream().filter(Future::failed).forEach(future -> {
                        log.error("Install bulk trigger: {}", future.cause().getMessage());
                        errors.add(future.cause().getMessage());
                    });
                    routingContext.response()
                            .setStatusCode(200)
                            .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                            .end(errors.encodePrettily());
                });
    }

    public static void main(String[] args) {
        String[] params = Arrays.copyOf(args, args.length + 1);
        params[params.length - 1] = MainVerticle.class.getName();
        Launcher.executeCommand("run", params);
    }

    private void serviceException(Throwable cause, RoutingContext routingContext) {
        if (cause instanceof ServiceException se) {
            if (se.failureCode() > 0) {
                routingContext.response().setStatusCode(se.failureCode()).end(se.getMessage());
            } else {
                routingContext.fail(cause);
            }
        } else {
            routingContext.fail(cause);
        }
    }

}
